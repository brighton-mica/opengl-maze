#include "Node.h"

// static definitions
unsigned int Node::wall_VAO, Node::wall_VBO, Node::uniform_model_location;
float Node::vertices[6];
void Node::init_gl(unsigned int model_location)
{
    // Set model
    uniform_model_location = model_location;

    // Set vertices so that line length is 1 node
    vertices[0] = 0.0f;
    vertices[1] = -0.5f;
    vertices[2] = 0.0f;
    vertices[3] = 0.0f;
    vertices[4] = 0.5f;
    vertices[5] = 0.0f;

    glGenVertexArrays(1, &wall_VAO);
    glBindVertexArray(wall_VAO);

    glGenBuffers(1, &wall_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, wall_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Node::Node(const int x, const int y, const int node_width, const int node_height, const int grid_x, const int grid_y)
{
    this->x = x;
    this->y = y;
    this->node_width = node_width;
    this->node_height = node_height;
    this->grid_location.first = grid_x;
    this->grid_location.second = grid_y;

    float half_node_width = node_width / 2.0f;
    float half_node_height = node_height / 2.0f;

    // initialize wall location
    wall_locations[0] = std::make_pair<float, float> (x, y + half_node_height); // UP
    wall_locations[1] = std::make_pair<float, float> (x, y - half_node_height); // DOWN
    wall_locations[2] = std::make_pair<float, float> (x - half_node_width, y); // LEFT
    wall_locations[3] = std::make_pair<float, float> (x + half_node_width, y); // RIGHT
}

void Node::remove_wall(int index)
{
    walls[index] = false;
}

void Node::render() const
{
    glm::mat4 model;
    glBindVertexArray(wall_VAO);

    // Note: in our VBO, the vertices are stored as a vertical line, so we must rotate the line
    //       by half_pi around the z-axis to make it horizontal for above and below. 

    if (walls[0]) // render above
    {
        // set model
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(wall_locations[0].first, wall_locations[0].second, 0.0f));
        model = glm::rotate(model, glm::half_pi<float>(), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(1.0f, node_width, 0.0f));
        glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));

        glDrawArrays(GL_LINES, 0, 2);
    }
    if (walls[1]) // render below
    {
        // set model
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(wall_locations[1].first, wall_locations[1].second, 0.0f));
        model = glm::rotate(model, glm::half_pi<float>(), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(1.0f, node_width, 0.0f));
        glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));

        glDrawArrays(GL_LINES, 0, 2);
    }
    if (walls[2])  // render left
    {
        // set model
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(wall_locations[2].first, wall_locations[2].second, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, node_height, 0.0f));
        glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));

        glDrawArrays(GL_LINES, 0, 2);
    }
    if (walls[3]) // render right
    {
        // set model
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(wall_locations[3].first, wall_locations[3].second, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, node_height, 0.0f));
        glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));

        glDrawArrays(GL_LINES, 0, 2);
    }

    glBindVertexArray(0);
}

std::pair<int,int> Node::get_grid_location() const { return grid_location; }
void Node::set_visited(bool val) { visited = val; }
bool Node::get_visited() const { return visited; }