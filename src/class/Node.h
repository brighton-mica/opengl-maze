#include <set>
#include <utility>

#include "glad/glad.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include "GLM/gtc/type_ptr.hpp"

class Node
{
    int x, y, node_width, node_height;
    std::pair<int,int> grid_location;
    bool visited = false;
    bool walls[4] = { true, true, true, true };

    /* wall_locations
     * Usage: Use to store the (x,y) transformation from (0,0) to the wall's location in 
     *        world space. Initlizated in constructor so calculating the location only 
     *        needs to happen once.
    */
    std::pair<float, float> wall_locations[4];

    // GL data
    static unsigned int wall_VAO, wall_VBO, uniform_model_location;
    static float vertices[];



    public:
        Node(const int x, const int y, const int node_width, const int node_height, const int grid_x, const int grid_y);

        /* init_gl()
         * Usage: used to init gl properties of the node class. The member is static as such 
         *        initialization only needs to happen once at the beginning of the program.
        */
        static void init_gl(unsigned int model_location);

        /* remove_wall()
         * Usage: Used in depth-first maze generation
         *        Takes in an integer which represents the direction of the wall to be removed
         *        UP: 0, DOWN: 1, LEFT: 2, RIGHT: 3
         * Runtime: O(1)
        */
        void remove_wall(int index);

        /* render()
         * Usage: Used in rendering the walls of a node
         * Issue: Seeing how there are overlapping walls, two neighboring nodes will share a similar
         *        wall, the wall will be rendered twice.
         * Runtime: O(4)
        */
        void render() const;

        /* get_grid_location()
         * Usage: Used in search alogrthms and maze generation algorithms to get neighboring nodes
        */
        std::pair<int,int> get_grid_location() const;

        void set_visited(bool val);
        bool get_visited() const;
};

