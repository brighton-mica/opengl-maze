#include <iostream>
#include <vector>
#include <memory>

#include <glad/glad.h>
#include "GLFW/glfw3.h"
#include "IMGUI/imgui.h"
#include "IMGUI/imgui_impl_glfw.h"
#include "IMGUI/imgui_impl_opengl3.h"

#include "Window.h"
#include "Shader.h"
#include "Node.h"

using std::cout;
using std::endl;

static int g_window_width = 1000;
static int g_window_height = 750;

static int g_nodes_x = 10;
static int g_nodes_y = 10;
std::vector<std::vector<std::shared_ptr<Node>>> g_grid;

static glm::mat4 g_projection;

static ImVec2 imgui_window_location(0.8f * g_window_width, 0);
static ImVec2 imgui_window_size(0.2f * g_window_width + 7, g_window_height);

// Functions
void window_resize_callback(GLFWwindow* window, const int width, const int height);
void create_grid();
void DFS(std::shared_ptr<Node> starting_node);

int main()
{
    

    // Initialize Window
    Window window(g_window_width, g_window_height, "Maze");
    window.set_resize_callback(window_resize_callback);
    glfwSetWindowSizeLimits(window.get_window(), 1000, 750, GLFW_DONT_CARE, GLFW_DONT_CARE);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window.get_window(), true);
    ImGui_ImplOpenGL3_Init("#version 330");
    bool changed_nodes_x = false;
    bool changed_nodes_y = false;
    bool maze_instant_generation = true;
    bool maze_generate = false;
    bool maze_generation_running = false;
    float maze_creation_speed = 1.0f;
    int maze_creation_seed = 0;

    // Initialize Shader
    Shader shader("./src/shaders/shader.vert", "./src/shaders/shader.frag");
    unsigned int uniform_projection_location = shader.get_uniform_location("u_projection");
    g_projection = glm::ortho(0.0f, (float)g_window_width, 0.0f, (float)g_window_height, -1.0f, 1.0f);
    
    /* Initialize Node
     * The Node class holds the logic for rendering walls; therefore, before rendering 
     * can begin we must create buffers and provide the appropriate uniforms.
    */
    Node::init_gl(shader.get_uniform_location("u_model"));
    
    // Initialize Grid
    create_grid();

    // -------------------------------
    //  Render Loop
    // -------------------------------

    while (!window.should_close())
    {
        glfwPollEvents();
        
        // -------------------------------
        //  Begin ImGui
        // -------------------------------
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();


        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);


        ImGui::Begin("Control Window");
        ImGui::SetWindowPos(imgui_window_location);
        ImGui::SetWindowSize(imgui_window_size);

        ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
        if (ImGui::BeginTabBar("tab-bar", tab_bar_flags))
        {
            if (ImGui::BeginTabItem("Maze"))
            {
                ImGui::BulletText("Size");
                ImGui::Indent(); 
                ImGui::SetNextItemWidth(75);
                changed_nodes_x = ImGui::SliderInt("# cells x", &g_nodes_x, 1, 100);
                ImGui::SetNextItemWidth(75);
                changed_nodes_y = ImGui::SliderInt("# cells y", &g_nodes_y, 1, 100);
                ImGui::Unindent();

                ImGui::BulletText("Creation");
                ImGui::Indent();
                if (maze_generate)
                {
                    if (!maze_instant_generation)
                    {
                        if (maze_generation_running)
                        {
                            maze_generation_running = !ImGui::Button("Stop");
                        }
                        else
                        {
                            maze_generation_running = ImGui::Button("Resume");
                        }
                        ImGui::SameLine();
                    }
                    if (ImGui::Button("Reset"))
                    {
                        create_grid();
                        maze_generate = false;
                        maze_generation_running = false;
                    }
                }
                else 
                {
                    if (ImGui::Button("Generate"))
                    {
                        // call dfs fresh
                        DFS(g_grid[0][0]);

                        maze_generate = true;
                        maze_generation_running = true;
                    }
                }
                
                // ImGui::SetNextItemWidth(75);
                // ImGui::Checkbox("Instant Generation", &maze_instant_generation);
                // ImGui::SetNextItemWidth(75);
                // ImGui::SliderFloat("Speed", &maze_creation_speed, 0.25f, 3.0f, "%.2f");

                ImGui::EndTabItem();
            }
            // if (ImGui::BeginTabItem("Searching"))
            // {
            //     ImGui::Text("This section deals with searching algorthms");
            //     ImGui::EndTabItem();
            // }
        }
        ImGui::EndTabBar();
        ImGui::End();
        

        if (changed_nodes_x)
        {
            create_grid();
            changed_nodes_x = false;
            maze_generate = false;
            maze_generation_running = false;
        } 
        else if (changed_nodes_y)
        {
            create_grid();
            changed_nodes_y = false;
            maze_generate = false;
            maze_generation_running = false;
        }
        // else if (maze_generate)
        // {
        //     maze_generate = false;
        // }

        // -------------------------------
        //  End ImGui
        // -------------------------------


        shader.use();
            glUniformMatrix4fv(uniform_projection_location, 1, GL_FALSE, glm::value_ptr(g_projection));
            for (auto row : g_grid)
            {
                for (auto node : row)
                {
                    node->render();
                }
            }
        glUseProgram(0);
        
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        window.swap_buffers();
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window.get_window());
    glfwTerminate();
    return 0;
}

void window_resize_callback(GLFWwindow* window, const int width, const int height)
{
    g_window_width = width;
    g_window_height = height;
    glViewport(0, 0, width, height);
    g_projection = glm::ortho(0.0f, (float)g_window_width, 0.0f, (float)g_window_height, -1.0f, 1.0f);
    imgui_window_location[0] = 0.8f * width;
    imgui_window_size[0] = 0.2f * width + 7;
    imgui_window_size[1] = height;
    create_grid();
}

void create_grid()
{
    // Wipe current grid
    g_grid.clear();

    float grid_container_width =  0.8f * g_window_width;
    
    /* 1. Calculate integer value for node size if grid is to take up 80% of container
     * 2. Calculate actual grid width with the node_width from (1)
     * 3. Subtract actual grid width from window container width to figure out padding
     * 4. Add half a node width to padding to ceneter node at location
    */
    int node_width = (0.9f * grid_container_width) / g_nodes_x;
    int actual_grid_width = node_width * g_nodes_x;
    float padding_x = (node_width / 2.0f) + (grid_container_width - actual_grid_width) / 2.0f;

    // Repeat for height
    int node_height = (0.9f * g_window_height) /  g_nodes_y;
    int actual_grid_height = node_height * g_nodes_y;
    float padding_y = (node_height / 2.0f) + (g_window_height - actual_grid_height) / 2.0f;
   
    for (int i = 0; i < g_nodes_x; i++)
    {
        std::vector<std::shared_ptr<Node>> row;
        g_grid.push_back(row);
        for (int j = 0; j < g_nodes_y; j++)
        {
            std::shared_ptr<Node> node = std::make_shared<Node>(padding_x + i * node_width, padding_y + j * node_height, node_width, node_height, i, j);
            g_grid[i].push_back(node);
        }
    }
}

std::vector<std::shared_ptr<Node>> get_neighbors(std::shared_ptr<Node> node)
{
    std::vector<std::shared_ptr<Node>> unvisited_neighbors;

    std::pair<int,int> location = node->get_grid_location();
    int x = location.first;
    int y = location.second;

    /* Check node above current node
     * 1. Make sure the above node is less than the number of rows in the grid
     * 2. Make sure the node has not been visited
    */
    if ((y+1) < g_nodes_y && !g_grid[x][y+1]->get_visited())
        unvisited_neighbors.push_back(g_grid[x][y+1]);

    /* Check node below current node
     * 1. Make sure the below node is greater than 0
     * 2. Make sure the node has not been visited
    */
    if ((y-1) >= 0 && !g_grid[x][y-1]->get_visited())
        unvisited_neighbors.push_back(g_grid[x][y-1]);

    /* Check node left of current node
     * 1. Make sure the left node is greater than 0
     * 2. Make sure the node has not been visited
    */
    if ((x-1) >= 0 && !g_grid[x-1][y]->get_visited())
        unvisited_neighbors.push_back(g_grid[x-1][y]);

    /* Check node right of current node
     * 1. Make sure the tighy node is less than the nu,ber of columns in the grid
     * 2. Make sure the node has not been visited
    */
    if ((x+1) < g_nodes_x && !g_grid[x+1][y]->get_visited())
        unvisited_neighbors.push_back(g_grid[x+1][y]);

    return unvisited_neighbors;
}

#include <stack>
void DFS(std::shared_ptr<Node> starting_node)
{
    std::stack<std::shared_ptr<Node>> stack;

    stack.push(starting_node);

    while (true)
    {
        // If the stack is empty, we have exhausted our search
        if (stack.empty())
        {
            cout << "DFS Maze Generation finished" << endl;
            break;
        }

        // Set current node
        std::shared_ptr<Node> current_node = stack.top();
        current_node->set_visited(true);
        
       

        // get neighbors
        std::vector<std::shared_ptr<Node>> neighbors = get_neighbors(current_node);
 
        // add random neighbor to stack
        if (neighbors.empty())
        {
            stack.pop();
            continue;
        }
            
        int index = rand() % neighbors.size();
        cout << index << endl;
        stack.push(neighbors[index]);
        // stack.push(neighbors[0]);
        int curr_x_location = current_node->get_grid_location().first;
        int curr_y_location = current_node->get_grid_location().second;
        int neigh_x_location = stack.top()->get_grid_location().first;
        int neigh_y_location = stack.top()->get_grid_location().second;

        // UP
        if (neigh_y_location > curr_y_location)
        {
            cout << "UP" << endl;
            stack.top()->remove_wall(1);
            current_node->remove_wall(0);
        }
        // BELOW
        else if (neigh_y_location < curr_y_location)
        {
            cout << "BELOW" << endl;
            std::cout << current_node->get_grid_location().first << " " << current_node->get_grid_location().second << endl;
            stack.top()->remove_wall(0);
            current_node->remove_wall(1);
        }
        // LEFT
        else if (neigh_x_location < curr_x_location)
        {
            cout << "LEFT" << endl;
            stack.top()->remove_wall(3);
            current_node->remove_wall(2);
        }
        // RIGHT
        else if (neigh_x_location > curr_x_location)
        {
            cout << "RIGHT" << endl;
            std::cout << current_node->get_grid_location().first << " " << current_node->get_grid_location().second << endl;
            stack.top()->remove_wall(2);
            current_node->remove_wall(3);
        }
    }
}