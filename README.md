This project shows how a maze can be generated from a 2D grid using depth-first search. 
![DFS Maze Generation](MAZE_GIF.gif)